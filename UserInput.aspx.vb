''JPW101518 completely redid subCargowise..
''JPW060314 include full path in output file..
''JPW050114 get image info from dp2..
Option Strict On
Option Explicit On
Imports System
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Net.WebClient
Imports System.ComponentModel

Namespace CDBurnFolders


    'Partial Class UserInput
    Partial Class Migrated_UserInput

        Inherits UserInput

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub
        Dim strConnString As String = System.Configuration.ConfigurationSettings.AppSettings("ConnString")
        Dim objConn As New System.Data.SqlClient.SqlConnection(strConnString)
        Dim objConn_DP2 As New System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("ConnStringDP2"))
        Dim objConn_EDI As New System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("ConnStringEDI"))

        Dim sOutputFileName As String = System.Configuration.ConfigurationSettings.AppSettings("OutputFileName")
        Dim sOutputFolder As String = System.Configuration.ConfigurationSettings.AppSettings("outputFolder")
        Dim sOutputFolderVax As String = System.Configuration.ConfigurationSettings.AppSettings("outputFolderVax")
        Dim sOutputFolderCw As String = System.Configuration.ConfigurationSettings.AppSettings("outputFolderCw")

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            'Put user code to initialize the page here

            If Not IsPostBack Then

                ''txtStartingCust.Text = "KEYAUT0001"
                ''txtEndingCust.Text = "KEYAUT0001"
                txtOutputFolderName.Text = "abcdefg"
                txtCust1.Text = "AL395"
                txt3461First.Text = "08/01/2014"
                txt3461Last.Text = "08/10/2014"

                Dim firstDay As DateTime = New DateTime(Date.Now.Year, Date.Now.Month, 1).AddMonths(-1)
                Dim lastDay As DateTime = New DateTime(Date.Now.Year, Date.Now.Month, 1).AddDays(-1)
                'txtCust1.Text = "MI620"

                'txt3461First.Text = firstDay.ToString("MM/dd/yyyy")
                'txt3461Last.Text = lastDay.ToString("MM/dd/yyyy")

                Call subshowhidefields()
            End If
        End Sub
        Private Sub subshowhidefields()
            phMessage.Visible = False
        End Sub
        Private Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinue.Click

            If Directory.Exists(sOutputFolder & "\" & txtOutputFolderName.Text) Then

                'Dim dirInfo As DirectoryInfo = New DirectoryInfo(sOutputFolder & "\" & txtOutputFolderName.Text)
                'For Each file As FileInfo In dirInfo.GetFiles()
                '    'Response.Write(file.Name())
                '    file.Delete()
                'Next
            Else
                Directory.CreateDirectory(sOutputFolder & "\" & txtOutputFolderName.Text)
            End If

            If fxValidateUserInput() Then
                ''If chkSystemQW.Checked = True Then
                Call subQuestaWeb()
                ''End If

                ''If chkSystemCW.Checked = True Then
                Call subCargowise()
                ''End If

                ''If chkSystemAL.Checked = True Then
                Call subAlliance()
                ''End If
            End If

        End Sub

        Private Sub subQuestaWeb()

            ''Dim sSubFolderName As String = ""
            ''sSubFolderName = fxGetSubfolderName(sSubFolderName)

            ''If sSubFolderName = "" Then Return

            Dim cmd As New SqlClient.SqlCommand
            Dim objAdapter As New System.Data.SqlClient.SqlDataAdapter
            Dim objDataset As New DataSet

            cmd.Connection = objConn_EDI
            cmd.CommandText = "cdImageBurn"
            cmd.CommandType = CommandType.StoredProcedure

            ''cmd.Parameters.Add("@argCustFirst", SqlDbType.VarChar).Value = txtStartingCust.Text
            ''cmd.Parameters.Add("@argCustLast", SqlDbType.VarChar).Value = txtEndingCust.Text
            cmd.Parameters.Add("@argCust1", SqlDbType.VarChar).Value = txtCust1.Text
            cmd.Parameters.Add("@argCust2", SqlDbType.VarChar).Value = txtCust2.Text
            cmd.Parameters.Add("@argCust3", SqlDbType.VarChar).Value = txtCust3.Text
            cmd.Parameters.Add("@argCust4", SqlDbType.VarChar).Value = txtCust4.Text
            cmd.Parameters.Add("@argCust5", SqlDbType.VarChar).Value = txtCust5.Text
            cmd.Parameters.Add("@argCust6", SqlDbType.VarChar).Value = txtCust6.Text
            cmd.Parameters.Add("@argCust7", SqlDbType.VarChar).Value = txtCust7.Text
            cmd.Parameters.Add("@argCust8", SqlDbType.VarChar).Value = txtCust8.Text
            cmd.Parameters.Add("@argCust9", SqlDbType.VarChar).Value = txtCust9.Text
            cmd.Parameters.Add("@argCust10", SqlDbType.VarChar).Value = txtCust10.Text
            cmd.Parameters.Add("@argDateFirst", SqlDbType.DateTime).Value = txt3461First.Text
            cmd.Parameters.Add("@argDateLast", SqlDbType.DateTime).Value = txt3461Last.Text
            ''cmd.Parameters.Add("@argSelectBy", SqlDbType.VarChar).Value = ddlSelectBy.SelectedValue
            cmd.Parameters.Add("@argDateType", SqlDbType.VarChar).Value = ddlDateType.SelectedValue
            cmd.Parameters.Add("@argShipmentType", SqlDbType.VarChar).Value = txtShipmentType.Text

            cmd.CommandTimeout = 14400

            Dim drImages As SqlDataReader

            objConn_EDI.Open()
            drImages = cmd.ExecuteReader

            If drImages.HasRows = True Then
                If Directory.Exists(sOutputFolder & "\" & txtOutputFolderName.Text) Then

                    'Dim dirInfo As DirectoryInfo = New DirectoryInfo(sOutputFolder & "\" & txtOutputFolderName.Text)
                    'For Each file As FileInfo In dirInfo.GetFiles()
                    '    'Response.Write(file.Name())
                    '    file.Delete()
                    'Next
                Else
                    Directory.CreateDirectory(sOutputFolder & "\" & txtOutputFolderName.Text)
                End If

                Dim prevFileNo As String = ""
                Dim currFileNo As String = ""
                Dim iImageCount As Integer = 0
                While drImages.Read()

                    currFileNo = drImages("ENTREFNO").ToString()
                    If currFileNo <> prevFileNo Then
                        iImageCount = 0
                    Else
                        iImageCount += 1
                    End If
                    prevFileNo = currFileNo

                    'Response.Write(drImages("ENTREFNO"))

                    Dim bw As BinaryWriter = Nothing

                    Dim sExtention As String = Path.GetExtension(drImages("PATH").ToString)

                    Dim sFile As String = sOutputFolder & "\" & txtOutputFolderName.Text & "\" & currFileNo & "_" & drImages("docCode").ToString & "_" & iImageCount.ToString & sExtention

                    bw = New BinaryWriter(New FileStream(sFile, FileMode.Create, FileAccess.Write))
                    Dim data() As Byte = CType(drImages.GetSqlBinary(drImages.GetOrdinal("data")), Byte())
                    bw.Write(data)
                    bw.Close()
                    bw = Nothing

                End While

                Dim alFolderNames As New ArrayList

                alFolderNames.Add(sOutputFolder & "\" & txtOutputFolderName.Text)

                Dim sFilePath As String = Server.MapPath(Request.ApplicationPath) & "\Output"

                Dim sResults As String
                OutFile.AL_to_Txt(alFolderNames, sFilePath, sOutputFileName, sResults)

                'Else

                '    lblMessage.Text = "No images found."
                '    phMessage.Visible = True

            End If

            lblMessage.Text = "Execution has completed, please review results and let IT staff know if you see any problems."
            phMessage.Visible = True

            drImages.Close()
            objConn_EDI.Close()

        End Sub
        Private Sub subCargowise()

            ''Dim sSubFolderName As String = ""
            ''sSubFolderName = fxGetSubfolderName(sSubFolderName)

            ''If sSubFolderName = "" Then Return

            Dim cmd As New SqlClient.SqlCommand
            Dim objAdapter As New System.Data.SqlClient.SqlDataAdapter
            Dim objDataset As New DataSet

            cmd.Connection = objConn_EDI
            cmd.CommandText = "cdImageBurnCW"
            cmd.CommandType = CommandType.StoredProcedure

            ''cmd.Parameters.Add("@argCustFirst", SqlDbType.VarChar).Value = txtStartingCust.Text
            ''cmd.Parameters.Add("@argCustLast", SqlDbType.VarChar).Value = txtEndingCust.Text
            cmd.Parameters.Add("@argCust1", SqlDbType.VarChar).Value = txtCust1.Text
            cmd.Parameters.Add("@argCust2", SqlDbType.VarChar).Value = txtCust2.Text
            cmd.Parameters.Add("@argCust3", SqlDbType.VarChar).Value = txtCust3.Text
            cmd.Parameters.Add("@argCust4", SqlDbType.VarChar).Value = txtCust4.Text
            cmd.Parameters.Add("@argCust5", SqlDbType.VarChar).Value = txtCust5.Text
            cmd.Parameters.Add("@argCust6", SqlDbType.VarChar).Value = txtCust6.Text
            cmd.Parameters.Add("@argCust7", SqlDbType.VarChar).Value = txtCust7.Text
            cmd.Parameters.Add("@argCust8", SqlDbType.VarChar).Value = txtCust8.Text
            cmd.Parameters.Add("@argCust9", SqlDbType.VarChar).Value = txtCust9.Text
            cmd.Parameters.Add("@argCust10", SqlDbType.VarChar).Value = txtCust10.Text
            cmd.Parameters.Add("@argDateFirst", SqlDbType.DateTime).Value = txt3461First.Text
            cmd.Parameters.Add("@argDateLast", SqlDbType.DateTime).Value = txt3461Last.Text
            ''cmd.Parameters.Add("@argSelectBy", SqlDbType.VarChar).Value = ddlSelectBy.SelectedValue
            cmd.Parameters.Add("@argDateType", SqlDbType.VarChar).Value = ddlDateType.SelectedValue
            cmd.Parameters.Add("@argShipmentType", SqlDbType.VarChar).Value = txtShipmentType.Text

            cmd.CommandTimeout = 14400

            Dim drImages As SqlDataReader

            objConn_EDI.Open()
            drImages = cmd.ExecuteReader

            If drImages.HasRows = True Then
                If Directory.Exists(sOutputFolderCw & "\" & txtOutputFolderName.Text) Then

                    ''Response.Write("directory exists")
                    ''Response.Write(sOutputFolderCw & "\" & sSubFolderName)
                    ''Return

                    'Dim dirInfo As DirectoryInfo = New DirectoryInfo(sOutputFolderCw & "\" & txtOutputFolderName.Text)
                    'For Each file As FileInfo In dirInfo.GetFiles()
                    '    'Response.Write(file.Name())
                    '    file.Delete()
                    'Next
                Else
                    Directory.CreateDirectory(sOutputFolderCw & "\" & txtOutputFolderName.Text)
                End If

                Dim prevFileNo As String = ""
                Dim currFileNo As String = ""
                Dim iImageCount As Integer = 0
                While drImages.Read()

                    currFileNo = drImages("ENTREFNO").ToString()
                    If currFileNo <> prevFileNo Then
                        iImageCount = 0
                    Else
                        iImageCount += 1
                    End If
                    prevFileNo = currFileNo

                    'Response.Write(drImages("ENTREFNO"))

                    Dim bw As BinaryWriter = Nothing

                    Dim sExtention As String = Path.GetExtension(drImages("PATH").ToString)

                    Dim sFile As String = sOutputFolderCw & "\" & txtOutputFolderName.Text & "\" & currFileNo & "_" & drImages("docCode").ToString & "_" & iImageCount.ToString & sExtention

                    bw = New BinaryWriter(New FileStream(sFile, FileMode.Create, FileAccess.Write))
                    Dim data() As Byte = CType(drImages.GetSqlBinary(drImages.GetOrdinal("data")), Byte())
                    bw.Write(data)
                    bw.Close()
                    bw = Nothing

                End While

                Dim alFolderNames As New ArrayList

                alFolderNames.Add(sOutputFolderCw & "\" & txtOutputFolderName.Text)

                Dim sFilePath As String = Server.MapPath(Request.ApplicationPath) & "\Output"

                Dim sResults As String
                OutFile.AL_to_Txt(alFolderNames, sFilePath, sOutputFileName, sResults)

            End If

            lblMessage.Text = "Execution has completed, please review results and let IT staff know if you see any problems."
            phMessage.Visible = True

            drImages.Close()
            objConn_EDI.Close()

        End Sub

        'Private Sub subCargowise()

        '    Dim sSubFolderName As String = ""
        '    sSubFolderName = fxGetSubfolderName(sSubFolderName)

        '    If sSubFolderName = "" Then Return

        '    Dim cmd As New SqlClient.SqlCommand
        '    Dim objAdapter As New System.Data.SqlClient.SqlDataAdapter
        '    Dim objDataset As New DataSet

        '    cmd.Connection = objConn_EDI
        '    cmd.CommandText = "cdImageBurnCW"
        '    cmd.CommandType = CommandType.StoredProcedure

        '    cmd.Parameters.Add("@argCustFirst", SqlDbType.VarChar).Value = txtStartingCust.Text
        '    cmd.Parameters.Add("@argCustLast", SqlDbType.VarChar).Value = txtEndingCust.Text
        '    cmd.Parameters.Add("@argCust1", SqlDbType.VarChar).Value = txtCust1.Text
        '    cmd.Parameters.Add("@argCust2", SqlDbType.VarChar).Value = txtCust2.Text
        '    cmd.Parameters.Add("@argCust3", SqlDbType.VarChar).Value = txtCust3.Text
        '    cmd.Parameters.Add("@argCust4", SqlDbType.VarChar).Value = txtCust4.Text
        '    cmd.Parameters.Add("@argCust5", SqlDbType.VarChar).Value = txtCust5.Text
        '    cmd.Parameters.Add("@argCust6", SqlDbType.VarChar).Value = txtCust6.Text
        '    cmd.Parameters.Add("@argCust7", SqlDbType.VarChar).Value = txtCust7.Text
        '    cmd.Parameters.Add("@argCust8", SqlDbType.VarChar).Value = txtCust8.Text
        '    cmd.Parameters.Add("@argCust9", SqlDbType.VarChar).Value = txtCust9.Text
        '    cmd.Parameters.Add("@argCust10", SqlDbType.VarChar).Value = txtCust10.Text
        '    cmd.Parameters.Add("@argDateFirst", SqlDbType.DateTime).Value = txt3461First.Text
        '    cmd.Parameters.Add("@argDateLast", SqlDbType.DateTime).Value = txt3461Last.Text
        '    cmd.Parameters.Add("@argSelectBy", SqlDbType.VarChar).Value = ddlSelectBy.SelectedValue
        '    cmd.Parameters.Add("@argDateType", SqlDbType.VarChar).Value = ddlDateType.SelectedValue

        '    cmd.CommandTimeout = 0

        '    Dim drImages As SqlDataReader

        '    objConn_EDI.Open()
        '    drImages = cmd.ExecuteReader()

        '    If drImages.HasRows = True Then
        '        If Directory.Exists(sOutputFolderCw & "\" & sSubFolderName) Then

        '            Dim dirInfo As DirectoryInfo = New DirectoryInfo(sOutputFolderCw & "\" & sSubFolderName)
        '            For Each file As FileInfo In dirInfo.GetFiles()
        '                'Response.Write(file.Name())
        '                file.Delete()
        '            Next
        '        Else
        '            Directory.CreateDirectory(sOutputFolderCw & "\" & sSubFolderName)
        '        End If

        '        Dim prevFileNo As String = ""
        '        Dim currFileNo As String = ""
        '        Dim iImageCount As Integer = 0
        '        Dim client As WebClient

        '        While drImages.Read()

        '            currFileNo = drImages("ENTREFNO").ToString()
        '            If currFileNo <> prevFileNo Then
        '                iImageCount = 0
        '            Else
        '                iImageCount += 1
        '            End If
        '            prevFileNo = currFileNo

        '            Dim sExtention As String = drImages("file_ext").ToString()
        '            Dim sFile As String = sOutputFolderCw & "\" & sSubFolderName & "\" & currFileNo & "_" & drImages("docCode").ToString & "_" & iImageCount.ToString & sExtention
        '            Dim id As String = drImages("id").ToString()

        '            ''fxDownloadFile(currFileNo, id, sFile)
        '            Response.Write(currFileNo)
        '        End While

        '        Dim alFolderNames As New ArrayList

        '        alFolderNames.Add(sOutputFolderCw & "\" & sSubFolderName)

        '        Dim sFilePath As String = Server.MapPath(Request.ApplicationPath) & "\Output"

        '        Dim sResults As String
        '        OutFile.AL_to_Txt(alFolderNames, sFilePath, sOutputFileName, sResults)
        '        lblMessage.Text = sResults
        '        phMessage.Visible = True

        '    Else

        '        lblMessage.Text = "No images found."
        '        phMessage.Visible = True

        '    End If

        '    drImages.Close()
        '    objConn_EDI.Close()

        'End Sub

        Private Sub subAlliance()

            ''Dim sSubFolderName As String = ""
            ''sSubFolderName = fxGetSubfolderName(sSubFolderName)

            ''If sSubFolderName = "" Then Return
            If txtOutputFolderName.Text = "" Then Return

            Dim dtFileNumbers As New DataTable

            subGetListOfFileNumbers(dtFileNumbers)

            If dtFileNumbers.Rows.Count <= 0 Then
                lblMessage.Text = "Execution has completed, please review results and let IT staff know if you see any problems."
                phMessage.Visible = True
                Exit Sub
            End If

            If Directory.Exists(sOutputFolderVax & "\" & txtOutputFolderName.Text) Then
                'Dim dirInfo As DirectoryInfo = New DirectoryInfo(sOutputFolderVax & "\" & sSubFolderName)
                'For Each file As FileInfo In dirInfo.GetFiles()
                '    'Response.Write(file.Name())
                '    file.Delete()
                'Next
            Else
                Directory.CreateDirectory(sOutputFolderVax & "\" & txtOutputFolderName.Text)
            End If

            Dim alFolderNames As New ArrayList
            Call subGetListOfFolderNames(dtFileNumbers, alFolderNames)
            'Call subGetFullPathList(dtFileNumbers, alFolderNames)

            If alFolderNames.Count <= 0 Then
                lblMessage.Text = "Array list returned 0 folder names."
                phMessage.Visible = True
                Exit Sub
            End If

            'Dim sFilePath As String = Server.MapPath(Request.ApplicationPath) & "\Output"

            'Dim sResults As String
            'OutFile.AL_to_Txt(alFolderNames, sFilePath, sOutputFileName, sResults)
            'lblMessage.Text = sResults
            'phMessage.Visible = True

            For iItem As Integer = 0 To alFolderNames.Count - 1
                Dim sFolder As String = CStr(alFolderNames.Item(iItem))
                sFolder = sFolder.Replace("memphis4", "docserver")
                'Response.Write(sFolder)
                'Response.Write("</BR>")
                If Directory.Exists(sFolder) Then
                    Dim dirInfo As DirectoryInfo = New DirectoryInfo(sFolder)
                    For Each file As FileInfo In dirInfo.GetFiles()
                        'Response.Write(file.FullName())
                        'Response.Write("</BR>")
                        file.CopyTo(sOutputFolderVax & "\" & txtOutputFolderName.Text & "\" & file.Name)
                    Next
                End If
            Next

            lblMessage.Text = "Execution has completed, please review results and let IT staff know if you see any problems."
            phMessage.Visible = True

        End Sub
        Private Sub subGetFullPathList( _
                ByVal argDT As DataTable, _
                ByRef argAL As ArrayList)

            Dim strSQL As String
            Dim sFileNo As String
            Dim drIdentity As SqlDataReader
            Dim sFullImagePath As String
            Dim sSlash1 As Int16
            Dim sSlash2 As Int16

            objConn_DP2.Open()

            '-----> Lines.
            For iRow As Integer = 0 To argDT.Rows.Count - 1

                sFileNo = CStr(argDT.Rows(iRow).Item("File_no"))
                'strSQL = "select FileLocation from openquery(mem2_cache,'" & _
                '    "select FileLocation from shipmentimage doc where FileNumber = " & sFileNo & "')"
                strSQL = "SELECT DISTINCT imagePath = FileLocation FROM CACHE_IMAGES doc(NOLOCK) WHERE FileNumber = " & sFileNo

                Dim cmd As SqlCommand = New SqlCommand(strSQL, objConn_DP2)
                cmd.CommandTimeout = 120

                drIdentity = cmd.ExecuteReader

                While drIdentity.Read()
                    argAL.Add(CStr(drIdentity("imagePath")))
                End While

                'If drIdentity.HasRows Then
                '    drIdentity.Read()
                '    sFullImagePath = CStr(drIdentity("FileLocation"))
                '    sSlash2 = CType(sFullImagePath.LastIndexOf("\"), Int16)
                '    sSlash1 = CType(sFullImagePath.Substring(0, sSlash2 - 1).LastIndexOf("\"), Int16)
                '    argAL.Add(sFullImagePath.Substring(sSlash1 + 1, sSlash2 - (sSlash1 + 1)))
                'End If
                drIdentity.Close()
            Next

            objConn_DP2.Close()

        End Sub


        Private Sub subGetListOfFolderNames( _
                ByVal argDT As DataTable, _
                ByRef argAL As ArrayList)

            Dim strSQL As String
            Dim sFileNo As String
            Dim drIdentity As SqlDataReader
            Dim sFullImagePath As String
            Dim sSlash1 As Int16
            Dim sSlash2 As Int16

            objConn_DP2.Open()

            '-----> Lines.
            For iRow As Integer = 0 To argDT.Rows.Count - 1

                sFileNo = CStr(argDT.Rows(iRow).Item("File_no"))
                'strSQL = "select FileLocation from openquery(mem2_cache,'" & _
                '    "select FileLocation from shipmentimage doc where FileNumber = " & sFileNo & "')"
                strSQL = "SELECT DISTINCT imagePath = SUBSTRING(FileLocation,1,LEN(FileLocation)-CHARINDEX('\',REVERSE(FileLocation))) FROM CACHE_IMAGES doc(NOLOCK) WHERE FileNumber = " & sFileNo

                Dim cmd As SqlCommand = New SqlCommand(strSQL, objConn_DP2)
                cmd.CommandTimeout = 120

                drIdentity = cmd.ExecuteReader

                While drIdentity.Read()
                    argAL.Add(CStr(drIdentity("imagePath")))
                End While

                'If drIdentity.HasRows Then
                '    drIdentity.Read()
                '    sFullImagePath = CStr(drIdentity("FileLocation"))
                '    sSlash2 = CType(sFullImagePath.LastIndexOf("\"), Int16)
                '    sSlash1 = CType(sFullImagePath.Substring(0, sSlash2 - 1).LastIndexOf("\"), Int16)
                '    argAL.Add(sFullImagePath.Substring(sSlash1 + 1, sSlash2 - (sSlash1 + 1)))
                'End If
                drIdentity.Close()
            Next

            objConn_DP2.Close()

        End Sub
        Private Sub subGetListOfFileNumbers(ByRef argDT As DataTable)

            If objConn.State = ConnectionState.Closed Then
                objConn.Open()
            End If

            Dim cmd As New SqlClient.SqlCommand
            Dim objAdapter As New System.Data.SqlClient.SqlDataAdapter
            Dim objDataset As New DataSet

            cmd.Connection = objConn
            cmd.CommandText = "spImageCDBurnFiles"
            cmd.CommandType = CommandType.StoredProcedure

            'objAdapter.SelectCommand = New System.Data.SqlClient.SqlCommand(strSQL, objConn)
            objAdapter.SelectCommand = cmd
            ''objAdapter.SelectCommand.Parameters.Add("@argCustFirst", SqlDbType.VarChar).Value = txtStartingCust.Text
            ''objAdapter.SelectCommand.Parameters.Add("@argCustLast", SqlDbType.VarChar).Value = txtEndingCust.Text
            objAdapter.SelectCommand.Parameters.Add("@argCust1", SqlDbType.VarChar).Value = txtCust1.Text
            objAdapter.SelectCommand.Parameters.Add("@argCust2", SqlDbType.VarChar).Value = txtCust2.Text
            objAdapter.SelectCommand.Parameters.Add("@argCust3", SqlDbType.VarChar).Value = txtCust3.Text
            objAdapter.SelectCommand.Parameters.Add("@argCust4", SqlDbType.VarChar).Value = txtCust4.Text
            objAdapter.SelectCommand.Parameters.Add("@argCust5", SqlDbType.VarChar).Value = txtCust5.Text
            objAdapter.SelectCommand.Parameters.Add("@argCust6", SqlDbType.VarChar).Value = txtCust6.Text
            objAdapter.SelectCommand.Parameters.Add("@argCust7", SqlDbType.VarChar).Value = txtCust7.Text
            objAdapter.SelectCommand.Parameters.Add("@argCust8", SqlDbType.VarChar).Value = txtCust8.Text
            objAdapter.SelectCommand.Parameters.Add("@argCust9", SqlDbType.VarChar).Value = txtCust9.Text
            objAdapter.SelectCommand.Parameters.Add("@argCust10", SqlDbType.VarChar).Value = txtCust10.Text
            objAdapter.SelectCommand.Parameters.Add("@argDate3461First", SqlDbType.DateTime).Value = txt3461First.Text
            objAdapter.SelectCommand.Parameters.Add("@argDate3461Last", SqlDbType.DateTime).Value = txt3461Last.Text
            ''objAdapter.SelectCommand.Parameters.Add("@argSelectBy", SqlDbType.VarChar).Value = ddlSelectBy.SelectedValue
            objAdapter.SelectCommand.CommandTimeout = 180

            ' Fill the dataset.
            objAdapter.Fill(objDataset, Cnst.DSTbl.FileNumbers)

            ' Create a new view.
            argDT = objDataset.Tables(Cnst.DSTbl.FileNumbers)

        End Sub
        Private Function fxValidateUserInput() As Boolean
            phMessage.Visible = False

            'If (Trim(txtStartingCust.Text) <> "" Or Trim(txtEndingCust.Text) <> "") And _
            '        (Trim(txtStartingCust.Text) = "" Or Trim(txtEndingCust.Text) = "") Then
            '    If Trim(txtStartingCust.Text) <> "" Then
            '        lblMessage.Text = "Starting customer has been entered, but ending customer is missing!"
            '    Else
            '        lblMessage.Text = "Ending customer has been entered, but starting customer is missing!"
            '    End If
            '    phMessage.Visible = True
            '    Return False
            'End If

            If (Trim(txtCust1.Text) = "" And _
                Trim(txtCust2.Text) = "" And _
                Trim(txtCust3.Text) = "" And _
                Trim(txtCust4.Text) = "" And _
                Trim(txtCust5.Text) = "" And _
                Trim(txtCust6.Text) = "" And _
                Trim(txtCust7.Text) = "" And _
                Trim(txtCust8.Text) = "" And _
                Trim(txtCust9.Text) = "" And _
                Trim(txtCust10.Text) = "") Then
                lblMessage.Text = "At least one customer code must be entered!"
                phMessage.Visible = True
                Return False
            End If

            If (Trim(txt3461First.Text) = String.Empty Or _
                Trim(txt3461Last.Text) = String.Empty) Then
                lblMessage.Text = "Date range must be filled in!"
                phMessage.Visible = True
                Return False
            End If

            Return True
        End Function

        Private Function fxGetSubfolderName(sSubFolderName As String) As String

            ''txtStartingCust.Text = txtStartingCust.Text.Trim
            ''txtEndingCust.Text = txtEndingCust.Text.Trim
            txtCust1.Text = txtCust1.Text.Trim
            txtCust2.Text = txtCust2.Text.Trim
            txtCust3.Text = txtCust3.Text.Trim
            txtCust4.Text = txtCust4.Text.Trim
            txtCust5.Text = txtCust5.Text.Trim
            txtCust6.Text = txtCust6.Text.Trim
            txtCust7.Text = txtCust7.Text.Trim
            txtCust8.Text = txtCust8.Text.Trim
            txtCust9.Text = txtCust9.Text.Trim
            txtCust10.Text = txtCust10.Text.Trim

            'If txtStartingCust.Text <> "" Then
            '    sSubFolderName = txtStartingCust.Text
            'ElseIf txtEndingCust.Text <> "" Then
            '    sSubFolderName = txtEndingCust.Text
            If txtCust1.Text <> "" Then
                sSubFolderName = txtCust1.Text
            ElseIf txtCust2.Text <> "" Then
                sSubFolderName = txtCust2.Text
            ElseIf txtCust3.Text <> "" Then
                sSubFolderName = txtCust3.Text
            ElseIf txtCust4.Text <> "" Then
                sSubFolderName = txtCust4.Text
            ElseIf txtCust5.Text <> "" Then
                sSubFolderName = txtCust5.Text
            ElseIf txtCust6.Text <> "" Then
                sSubFolderName = txtCust6.Text
            ElseIf txtCust7.Text <> "" Then
                sSubFolderName = txtCust7.Text
            ElseIf txtCust8.Text <> "" Then
                sSubFolderName = txtCust8.Text
            ElseIf txtCust9.Text <> "" Then
                sSubFolderName = txtCust9.Text
            ElseIf txtCust10.Text <> "" Then
                sSubFolderName = txtCust10.Text
            End If

            Return sSubFolderName

        End Function


        Private Sub fxDownloadFile(fileNo As String, id As String, filePath As String)
            Dim cmd As SqlCommand = New SqlCommand("util_cw_spGetShipmentDocumentById", objConn_DP2)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@arg_vcFILE_NO", SqlDbType.VarChar, 20).Value = fileNo
            cmd.Parameters.Add("@arg_vcId", SqlDbType.VarChar, 200).Value = id

            cmd.CommandTimeout = 0

            objConn_DP2.Open()

            Dim rd As SqlDataReader = cmd.ExecuteReader()
            Dim bw As BinaryWriter

            While (rd.Read())
                Try
                    bw = New BinaryWriter(New FileStream(filePath, FileMode.Create, FileAccess.Write))
                    Dim data As Byte() = CType(rd.GetSqlBinary(rd.GetOrdinal("imagedata")), Byte())
                    bw.Write(data)
                Finally
                    If bw IsNot Nothing Then bw.Close()
                End Try
            End While

            objConn_DP2.Close()
        End Sub

    End Class

End Namespace
