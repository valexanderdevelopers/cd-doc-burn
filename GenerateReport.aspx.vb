Namespace CDBurnFolders

Partial Class WebForm1
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

    Dim strConnString As String = ConfigurationSettings.AppSettings("ConnString")
    Dim sDBAllianceSync As String = ConfigurationSettings.AppSettings("dbAllianceSync")

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        ' Create a connection and open it.
        'Dim objConn As New System.Data.SqlClient.SqlConnection("User ID=sa;Password=vacoadmin15!;Initial Catalog=AllianceSync;Data Source=memphis4.valexander.com;")

        Dim objConn As New System.Data.SqlClient.SqlConnection(strConnString)
        objConn.Open()

        Dim sReportTitle As String
        Dim sDivParameters As String
        Dim sTeamParameters As String
        Dim sDateParameters As String

        Dim strSQL As String
        Dim objDataset As New DataSet
        Dim objAdapter As New System.Data.SqlClient.SqlDataAdapter

        If Session("DOSystem") = "PC" Then
            strSQL = "exec " & sDBAllianceSync & ".dbo.spTruckerPlanningPC @StartDiv, @EndDiv, @StartTeam, @EndTeam, @StartETAPOE, @EndETAPOE "
        ElseIf Session("DOSystem") = "VAX" Then
            strSQL = "exec " & sDBAllianceSync & ".dbo.spTruckerPlanning @StartDiv, @EndDiv, @StartTeam, @EndTeam, @StartETAPOE, @EndETAPOE "
        Else
            Response.Write("Invalid System")
            Response.End()
        End If

        'strSQL = "Select * from customers where country='USA'"

        objAdapter.SelectCommand = New System.Data.SqlClient.SqlCommand(strSQL, objConn)
        objAdapter.SelectCommand.Parameters.Add("@StartDiv", Session("StartingDivision"))
        objAdapter.SelectCommand.Parameters.Add("@EndDiv", Session("EndingDivision"))
        objAdapter.SelectCommand.Parameters.Add("@StartTeam", Session("StartingTeam"))
        objAdapter.SelectCommand.Parameters.Add("@EndTeam", Session("EndingTeam"))
        objAdapter.SelectCommand.Parameters.Add("@StartETAPOE", Session("StartingETAPOE"))
        objAdapter.SelectCommand.Parameters.Add("@EndETAPOE", Session("EndingETAPOE"))
        objAdapter.SelectCommand.CommandTimeout = 120

        ' Fill the dataset.
        objAdapter.Fill(objDataset)

        ' Create a new view.
        Dim oView As New DataView(objDataset.Tables(0))
        ' Set up the data grid and bind the data.

        ' There can be multiple DOs on a file, so get only that which has the most recent DO print date.
        oView.RowFilter = " DATE_TIME_ENTERED_IN_MINUTES = DATE_TIME_ENTERED_IN_MINUTES_MAX "

        DataGrid1.DataSource = oView
        DataGrid1.DataBind()

        ' Set the content type to Excel.
        Response.ContentType = "application/vnd.ms-excel"
        ' Remove the charset from the Content-Type header.
        Response.Charset = ""
        ' Turn off the view state.
        Me.EnableViewState = False

        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)
        Dim sStyle As String

        sStyle = "<style> " & _
            "@page  " & _
            "	{margin:.5in 0in .5in 0in; " & _
            "	mso-header-margin:.5in; " & _
            "	mso-footer-margin:.5in; " & _
            "	mso-page-orientation:landscape;} " & _
            ".fileno {width:54;} " & _
            ".containerno {width:96;} " & _
            ".dono {width:33;} " & _
            ".poe {width:47;} " & _
            ".dispatchpoi {width:68;} " & _
            ".etapoe {width:68;} " & _
            ".atapoe {width:68;} " & _
            ".etafat {width:68;} " & _
            ".etadest {width:68;} " & _
            ".scac {width:47;} " & _
            ".trucker {width:89;} " & _
            ".raillocation {width:152;} " & _
            ".deliverycity {width:96;} " & _
            ".customername {width:145;} " & _
            ".customsrelease {width:33;} " & _
            ".ogarelease {width:33;} " & _
            ".teamcode {width:47;} " & _
            ".conceptcode {width:68;} " & _
            "</style>"

        Call subDatagridApplyStyles(DataGrid1, "fileno", 2)
        Call subDatagridApplyStyles(DataGrid1, "containerno", 3)
        Call subDatagridApplyStyles(DataGrid1, "dono", 4)
        Call subDatagridApplyStyles(DataGrid1, "poe", 5)
        Call subDatagridApplyStyles(DataGrid1, "dispatchpoi", 6)
        Call subDatagridApplyStyles(DataGrid1, "etapoe", 7)
        Call subDatagridApplyStyles(DataGrid1, "atapoe", 8)
        Call subDatagridApplyStyles(DataGrid1, "etafat", 9)
        Call subDatagridApplyStyles(DataGrid1, "etadest", 10)
        Call subDatagridApplyStyles(DataGrid1, "scac", 11)
        Call subDatagridApplyStyles(DataGrid1, "trucker", 12)
        Call subDatagridApplyStyles(DataGrid1, "raillocation", 13)
        Call subDatagridApplyStyles(DataGrid1, "deliverycity", 14)
        Call subDatagridApplyStyles(DataGrid1, "customername", 15)
        Call subDatagridApplyStyles(DataGrid1, "customsrelease", 16)
        Call subDatagridApplyStyles(DataGrid1, "ogarelease", 17)
        Call subDatagridApplyStyles(DataGrid1, "teamcode", 18)
        Call subDatagridApplyStyles(DataGrid1, "conceptcode", 19)

        sReportTitle = Date.Now.ToString & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Trucker Planning Report</b>"
        sDivParameters = "Divisions: " & IIf(Session("StartingDivision") = "", "First", Session("StartingDivision")) & " to " & IIf(Session("EndingDivision") = "", "Last", Session("EndingDivision"))
        sTeamParameters = "Teams: " & IIf(Session("StartingTeam") = "", "First", Session("StartingTeam")) & " to " & IIf(Session("EndingTeam") = "", "Last", Session("EndingTeam"))
        sDateParameters = "ETAPOE: " & IIf(Session("StartingETAPOE") = "", "First", Session("StartingETAPOE")) & " to " & IIf(Session("EndingETAPOE") = "", "Last", Session("EndingETAPOE"))

        ' Get the HTML for the control.
        DataGrid1.RenderControl(hw)
        ' Write the HTML back to the browser.
        Response.Write(sStyle)
        Response.Write(sReportTitle & "<BR>")
        Response.Write(sDivParameters & "&nbsp;&nbsp;&nbsp;&nbsp;" & sTeamParameters & "&nbsp;&nbsp;&nbsp;&nbsp;" & sDateParameters & "<BR>")
        Response.Write(tw.ToString())
        ' End the response.
        Response.End()
    End Sub
    Private Sub subDatagridApplyStyles(ByRef argDataGrid As DataGrid, ByVal argClassName As String, ByVal argColumn As Integer)

        Dim rowPos As Integer
        For rowPos = 0 To argDataGrid.Items.Count - 1
            ' Loop through the cols and set the horizontal alignment
            Dim colPos As Integer
            argDataGrid.Items(rowPos).Cells(argColumn).Attributes.Add("class", argClassName)
        Next

    End Sub
    Private Sub DataGrid1_ItemDataBound(ByVal s As Object, ByVal e As DataGridItemEventArgs) Handles DataGrid1.ItemDataBound
        e.Item.Cells(0).Visible = False
        e.Item.Cells(1).Visible = False
    End Sub
End Class

End Namespace
