Imports System.IO
Imports CDBurnFolders


Namespace CDBurnFolders

Public Module OutFile
    Public Sub AL_to_Txt( _
        ByVal argAL As ArrayList, _
        ByVal argFilePath As String, _
        ByVal argFileName As String, _
        ByRef argResults As String)
        Try

            Dim dr As DataRow, ary() As Object
            Dim iRow As Integer, iCol As Integer
            Dim sFile As String
            Dim sCSV_Record As String
                Dim uc As UserInput

            sFile = argFilePath & "\" & argFileName

            If File.Exists(sFile) Then
                File.Delete(sFile)
            End If

            Dim sr As StreamWriter = File.CreateText(sFile)

            For iItem As Integer = 0 To argAL.Count - 1
                sr.WriteLine(argAL.Item(iItem))
            Next

            sr.Close()
            sr = Nothing

            argResults = "Folder list has been successfully generated.<br>"
            argResults += "Qualifying folders can be found in file " & sFile
        Catch ex As Exception
            argResults = "There has been and error in generating the folder list.<br>"
            argResults += "Error: " & ex.Message
        End Try

    End Sub
End Module

End Namespace
