<%@ Page Language="vb" AutoEventWireup="false" Inherits="CDBurnFolders.Migrated_UserInput" CodeFile="UserInput.aspx.vb" Async="true" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>UserInput</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
</head>
<body>
    <form id="frmUserInput" method="post" runat="server">
        <asp:Label ID="Label6" Style="z-index: 100; left: 72px; position: absolute; top: 304px" runat="server"
            Font-Bold="True" Width="181px" Height="32px">Stopping Date:</asp:Label>
        <asp:Label ID="Label5" Style="z-index: 101; left: 72px; position: absolute; top: 280px" runat="server"
            Font-Bold="True" Width="190px" Height="24px">Starting Date:</asp:Label>
        <asp:Label ID="Label3" Style="z-index: 102; left: 72px; position: absolute; top: 152px" runat="server"
            Font-Bold="True" Width="169px" Height="32px">Customer Codes:</asp:Label>
        <asp:Label ID="Label1" Style="z-index: 104; left: 72px; position: absolute; top: 96px" runat="server"
            Font-Bold="True" Width="173px" Height="24px">Output Folder Name:</asp:Label>
        <asp:Label ID="Label2" Style="z-index: 104; left: 72px; position: absolute; top: 124px" runat="server"
            Font-Bold="True" Width="173px" Height="24px">Shipment Type:</asp:Label>
        <asp:TextBox ID="txtOutputFolderName" Style="z-index: 106; left: 256px; position: absolute; top: 96px"
            Width="150px" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtShipmentType" Style="z-index: 106; left: 256px; position: absolute; top: 124px"
            Width="150px" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCust1" Style="z-index: 108; left: 256px; position: absolute; top: 152px"
            Width="150px" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCust2" Style="z-index: 116; left: 410px; position: absolute; top: 152px"
            Width="150px" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCust3" Style="z-index: 114; left: 256px; position: absolute; top: 176px"
            Width="150px" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCust4" Style="z-index: 109; left: 410px; position: absolute; top: 176px"
            Width="150px" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCust5" Style="z-index: 117; left: 256px; position: absolute; top: 200px"
            Width="150px" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCust6" Style="z-index: 115; left: 410px; position: absolute; top: 200px"
            Width="150px" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCust7" Style="z-index: 118; left: 256px; position: absolute; top: 224px"
            Width="150px" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCust8" Style="z-index: 119; left: 410px; position: absolute; top: 224px"
            Width="150px" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCust9" Style="z-index: 120; left: 256px; position: absolute; top: 248px"
            Width="150px" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCust10" Style="z-index: 121; left: 410px; position: absolute; top: 248px"
            Width="150px" runat="server"></asp:TextBox>
        <asp:TextBox ID="txt3461First" Style="z-index: 110; left: 256px; position: absolute; top: 280px"
            runat="server"></asp:TextBox>
        &nbsp;
           
        <asp:TextBox ID="txt3461Last" Style="z-index: 111; left: 256px; position: absolute; top: 304px"
            runat="server">
        </asp:TextBox>
        <asp:Label ID="lblTitle" Style="z-index: 113; left: 176px; position: absolute; top: 16px" runat="server"
            Font-Bold="True" Width="288px" Height="32px" Font-Size="Larger">Files/Folder for Burn to CD/DVD</asp:Label>
        <table style="z-index: 112; left: 184px; position: absolute; top: 385px">
            <tr>
                <asp:PlaceHolder ID="phMessage" runat="server" Visible="False">
                    <td>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                </asp:PlaceHolder>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnContinue" runat="server" Width="179px" Text="Generate Folder List"></asp:Button></td>
            </tr>
            <tr>
                <td style="font-size: 18px">Important Note:<br />
                    Alliance/VAX customer codes are 5 characters, e.g. BR360<br />
                    Questaweb customer codes are 8 characters, e.g. BRAM3600<br />
                    Cargowise customer codes are more than 8, e.g. ALLTIR0006<br />
                    ----------------------------------------------------------------------<br />
                    Shipment Type are 3 character codes, e.g: EXP, IMP and so on<br />
                    If you do not want to filter by Shipment Type, simply leave the field empty<br />
                </td>
            </tr>
        </table>
        <asp:Label ID="Label7" Style="z-index: 100; left: 72px; position: absolute; top: 328px" runat="server"
            Font-Bold="True" Width="181px" Height="32px">Date Type:</asp:Label>
        <asp:DropDownList ID="ddlDateType" Style="z-index: 124; left: 256px; position: absolute; top: 328px"
            runat="server" Height="24px" Width="152px">
            <asp:ListItem Value="FILED3461">Entry Filed with CBP</asp:ListItem>
            <asp:ListItem Value="LASTBILLED" Selected="True">Date Billed</asp:ListItem>
            <asp:ListItem Value="CUSTOMSRELEASED">Release Date</asp:ListItem>
        </asp:DropDownList>
    </form>
    <script language="javascript" src="inc/dlcalendar.js"></script>
</body>
</html>
